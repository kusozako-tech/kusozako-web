
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

CREATE_WEB_PAGE = 0         # uri(str)
BOOKMARK_WEB_PAGE = 1       # uri(str), title(str)
FINALIZE = 2                # None
