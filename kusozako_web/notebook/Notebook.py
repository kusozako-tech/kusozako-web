
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_web.startup_page.StartupPage import DeltaStartupPage
from kusozako_web import InterPageSignals
from .actions.Actions import EchoActions


class DeltaNotebook(Gtk.Notebook, DeltaEntity):

    def _delta_call_add_to_notebook(self, user_data):
        page_widget, label_widget = user_data
        current_page_index = self.get_current_page()
        self.insert_page(page_widget, label_widget, current_page_index+1)
        self.set_tab_reorderable(page_widget, True)
        self.show_all()

    def _delta_call_remove_page(self, page_content):
        self.detach_tab(page_content)
        page_content.destroy()

    def _delta_info_notebook(self):
        return self

    def _on_destroy_event(self, notebook):
        param = InterPageSignals.FINALIZE, None
        self._raise("delta > inter page signal", param)

    def _on_change_current_page(self, notebook, page, page_number):
        label = self.get_tab_label(page)
        text = label.get_text() + "- kusozako-web"
        self._raise("delta > application window title", text)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Notebook.__init__(self)
        DeltaStartupPage(self)
        EchoActions(self)
        self.connect("destroy", self._on_destroy_event)
        self._raise("delta > add to container", self)
        self.connect("switch-page", self._on_change_current_page)
