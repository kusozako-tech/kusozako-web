
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Create import DeltaCreate


class EchoActions:

    def __init__(self, parent):
        DeltaCreate(parent)
