
# (c) copyright 2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .box.Box import DeltaBox


class DeltaStartupPage(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        user_data = DeltaBox(self), Gtk.Label("Start")
        self._raise("delta > add to notebook", user_data)
