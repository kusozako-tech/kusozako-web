
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .base_model.BaseModel import DeltaBaseModel
from .icon_view.IconView import DeltaIconView


class DeltaBookmark(DeltaEntity):

    def _delta_info_model(self):
        return self._model

    def __init__(self, parent):
        self._parent = parent
        self._model = DeltaBaseModel(self)
        DeltaIconView(self)
