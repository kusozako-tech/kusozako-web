
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_web import BookmarkColumns
from .watchers.Watchers import EchoWatchers


class DeltaIconView(Gtk.IconView, DeltaEntity):

    def _delta_info_icon_view(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(vexpand=True)
        Gtk.IconView.__init__(
            self,
            model=self._enquiry("delta > model"),
            pixbuf_column=BookmarkColumns.PIXBUF,
            text_column=BookmarkColumns.TITLE,
            item_width=Unit(12)
            )
        scrolled_window.add(self)
        EchoWatchers(self)
        self._raise("delta > add to container", scrolled_window)
