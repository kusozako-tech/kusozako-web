
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_web import InterPageSignals


class DeltaBookmarkWebPage(DeltaEntity):

    def _action(self, param):
        # uri, title = param
        self._raise("delta > append bookmark", param)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == InterPageSignals.BOOKMARK_WEB_PAGE:
            self._action(param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register inter page object", self)
