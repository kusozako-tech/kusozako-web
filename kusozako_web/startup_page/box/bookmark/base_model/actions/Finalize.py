
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_web import InterPageSignals
from kusozako_web import BookmarkColumns
from kusozako_web import Path


class DeltaFinalize(DeltaEntity):

    def _action(self, param=None):
        key_file = GLib.KeyFile.new()
        for tree_row in self._enquiry("delta > model"):
            uri = tree_row[BookmarkColumns.URI]
            title = tree_row[BookmarkColumns.TITLE]
            key_file.set_string("bookmark", title, uri)
        key_file.save_to_file(Path.BOOKMARK_FILE)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == InterPageSignals.FINALIZE:
            self._action(param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register inter page object", self)
