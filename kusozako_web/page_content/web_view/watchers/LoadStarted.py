
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import WebKit2
from .Load import AlfaLoad


class DeltaLoadStarted(AlfaLoad):

    LOAD_EVENT = WebKit2.LoadEvent.STARTED

    def _on_event(self, web_view):
        print("started !!")
