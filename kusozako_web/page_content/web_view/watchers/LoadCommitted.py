
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import WebKit2
from kusozako_web import WebPageSignals
from .Load import AlfaLoad


class DeltaLoadCommitted(AlfaLoad):

    LOAD_EVENT = WebKit2.LoadEvent.COMMITTED

    def _on_event(self, web_view):
        progress = web_view.get_estimated_load_progress()
        # print("favicon ?", web_view.get_favicon())
        # print("title ?", web_view.get_title())
        param = WebPageSignals.LOAD_COMMITED, progress
        self._raise("delta > web page signal", param)
