
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .LoadStarted import DeltaLoadStarted
from .LoadCommitted import DeltaLoadCommitted
from .LoadFinished import DeltaLoadFinished
from .Create import DeltaCreate


class EchoWatchers:

    def __init__(self, parent):
        DeltaLoadStarted(parent)
        DeltaLoadCommitted(parent)
        DeltaLoadFinished(parent)
        DeltaCreate(parent)
