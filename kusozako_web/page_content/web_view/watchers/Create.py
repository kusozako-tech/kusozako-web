
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_web import InterPageSignals


class DeltaCreate(DeltaEntity):

    def _on_create(self, web_view, navigation_action):
        uri_request = navigation_action.get_request()
        uri = uri_request.get_uri()
        param = InterPageSignals.CREATE_WEB_PAGE, uri
        self._raise("delta > inter page signal", param)

    def __init__(self, parent):
        self._parent = parent
        web_view = self._enquiry("delta > web view")
        web_view.connect("create", self._on_create)
