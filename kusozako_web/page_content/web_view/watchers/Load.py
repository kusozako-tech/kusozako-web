
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class AlfaLoad(DeltaEntity):

    LOAD_EVENT = "define load event to handle here."

    def _on_event(self, web_view):
        raise NotImplementedError

    def _on_load_changed(self, web_view, load_event):
        if load_event == self.LOAD_EVENT:
            self._on_event(web_view)

    def __init__(self, parent):
        self._parent = parent
        web_view = self._enquiry("delta > web view")
        web_view.connect("load-changed", self._on_load_changed)
