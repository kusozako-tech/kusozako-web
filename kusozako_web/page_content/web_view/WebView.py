
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import WebKit2
from libkusozako3.Entity import DeltaEntity
from .actions.Actions import EchoActions
from .watchers.Watchers import EchoWatchers


class DeltaWebView(WebKit2.WebView, DeltaEntity):

    def _delta_info_web_view(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        settings = WebKit2.Settings(
            enable_javascript=False,
            enable_smooth_scrolling=True
            )
        WebKit2.WebView.__init__(self, settings=settings)
        self.load_plain_text("")
        EchoActions(self)
        EchoWatchers(self)
