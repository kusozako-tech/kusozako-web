
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .label.Label import DeltaLabel
from .web_view.WebView import DeltaWebView


class DeltaPageContent(DeltaEntity):

    @classmethod
    def new_for_uri(cls, parent, uri):
        page_content = cls(parent)
        page_content.initialize(uri)

    def _delta_call_register_web_page_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_web_page_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_close_tab(self):
        self._raise("delta > remove page", self._web_view)

    def initialize(self, uri):
        label = DeltaLabel(self)
        self._web_view = DeltaWebView(self)
        user_data = self._web_view, label
        self._raise("delta > add to notebook", user_data)
        GLib.idle_add(self._web_view.load_uri, uri)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
