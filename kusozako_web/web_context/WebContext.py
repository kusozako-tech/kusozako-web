
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import WebKit2
from libkusozako3.Entity import DeltaEntity
from kusozako_web import Path


class DeltaWebContext(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        self._context = WebKit2.WebContext.get_default()
        self._context.set_favicon_database_directory(Path.FAVICON_DIRECTORY)
