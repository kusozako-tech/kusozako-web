
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_web import WebPageSignals
from kusozako_web import InterPageSignals


class DeltaBookmarkThisTab(DeltaEntity):

    def _action(self, param=None):
        web_view = self._enquiry("delta > web view")
        signal_param = web_view.get_uri(), web_view.get_title()
        param = InterPageSignals.BOOKMARK_WEB_PAGE, signal_param
        self._raise("delta > inter page signal", param)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == WebPageSignals.BOOKMARK_THIS_TAB:
            self._action(param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register web page object", self)
