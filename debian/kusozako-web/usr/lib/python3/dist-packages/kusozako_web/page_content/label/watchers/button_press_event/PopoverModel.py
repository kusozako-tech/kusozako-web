
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_web import WebPageSignals


MAIN_PAGE = {
    "page-name": "main",
    "items": [
        {
            "type": "simple-action",
            "title": _("Bookmark This Tab"),
            "message": "delta > web page signal",
            "user-data": (WebPageSignals.BOOKMARK_THIS_TAB, None),
            "close-on-clicked": True
        },
        {
            "type": "separator"
        },
        {
            "type": "simple-action",
            "title": _("Close Tab"),
            "message": "delta > close tab",
            "user-data": None,
            "close-on-clicked": True
        }
    ]
}

POPOVER_MODEL = [MAIN_PAGE]
