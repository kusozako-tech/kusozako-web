
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .button_press_event.ButtonPressEvent import DeltaButtonPressEvent


class EchoWatchers:

    def __init__(self, parent):
        DeltaButtonPressEvent(parent)
