
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from libkusozako3.Entity import DeltaEntity
from kusozako_web import WebPageSignals
from .watchers.Watchers import EchoWatchers


class DeltaLabel(Gtk.EventBox, DeltaEntity):

    def get_text(self):
        return self._label.get_text()

    def receive_transmission(self, user_data):
        signal, title = user_data
        if signal == WebPageSignals.LOAD_COMMITED:
            self._label.set_text("{:.2%}".format(title))
        if signal == WebPageSignals.LOAD_FINISHED:
            self._label.set_text(title)
            self._label.props.tooltip_text = title

    def _delta_info_event_source(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventBox.__init__(self, hexpand=True)
        self._label = Gtk.Label(
            "loading...",
            ellipsize=Pango.EllipsizeMode.MIDDLE
            )
        self.add(self._label)
        self.show_all()
        EchoWatchers(self)
        self._raise("delta > register web page object", self)
