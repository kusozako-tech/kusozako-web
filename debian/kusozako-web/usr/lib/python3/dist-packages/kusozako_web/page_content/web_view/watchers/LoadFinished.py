
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import WebKit2
from kusozako_web import WebPageSignals
from .Load import AlfaLoad


class DeltaLoadFinished(AlfaLoad):

    LOAD_EVENT = WebKit2.LoadEvent.FINISHED

    def _on_event(self, web_view):
        title = web_view.get_title()
        if title is None:
            GLib.timeout_add_seconds(1, self._on_event, web_view)
        param = WebPageSignals.LOAD_FINISHED, web_view.get_title()
        self._raise("delta > web page signal", param)
        return GLib.SOURCE_REMOVE
