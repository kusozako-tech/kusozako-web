
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_web import InterPageSignals
from kusozako_web.page_content.PageContent import DeltaPageContent


class DeltaCreate(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, uri = user_data
        if signal != InterPageSignals.CREATE_WEB_PAGE:
            return
        notebook = self._enquiry("delta > notebook")
        DeltaPageContent.new_for_uri(notebook, uri)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register inter page object", self)
