
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_web import Path


class DeltaInitializer(DeltaEntity):

    def _load_from_file(self):
        key_file = GLib.KeyFile.new()
        key_file.load_from_file(Path.BOOKMARK_FILE, GLib.KeyFileFlags.NONE)
        titles, _ = key_file.get_keys("bookmark")
        for title in titles:
            uri = key_file.get_string("bookmark", title)
            self._raise("delta > append bookmark", (uri, title))

    def _set_default(self):
        user_data = "https://duckduckgo.com/", "duck duck go"
        self._raise("delta > append bookmark", user_data)

    def __init__(self, parent):
        self._parent = parent
        if GLib.file_test(Path.BOOKMARK_FILE, GLib.FileTest.EXISTS):
            self._load_from_file()
        else:
            self._set_default()
