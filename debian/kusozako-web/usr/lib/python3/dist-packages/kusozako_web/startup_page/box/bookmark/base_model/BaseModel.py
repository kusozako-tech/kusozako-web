
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_web import BookmarkColumns
from .Initializer import DeltaInitializer
from .DataConstructor import DeltaDataConstructor
from .actions.Actions import EchoActions


class DeltaBaseModel(Gtk.ListStore, DeltaEntity):

    def _delta_call_append_tree_row_data(self, tree_row_data):
        self.append(tree_row_data)

    def _delta_call_append_bookmark(self, user_data):
        # uri, title = user_data
        self._data_constructor.construct(user_data)

    def __init__(self, parent):
        self._parent = parent
        self._data_constructor = DeltaDataConstructor(self)
        Gtk.ListStore.__init__(self, *BookmarkColumns.TYPES)
        self.set_sort_column_id(
            BookmarkColumns.LAST_ACCESSED,
            Gtk.SortType.DESCENDING
            )
        EchoActions(self)
        DeltaInitializer(self)
