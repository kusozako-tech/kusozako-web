
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_web import BookmarkColumns
from kusozako_web import InterPageSignals


class DeltaItemActivated(DeltaEntity):

    def _on_item_activated(self, icon_view, tree_path):
        model = icon_view.get_model()
        tree_row = model[tree_path]
        uri = tree_row[BookmarkColumns.URI]
        param = InterPageSignals.CREATE_WEB_PAGE, uri
        self._raise("delta > inter page signal", param)
        now = GLib.DateTime.new_now_local()
        tree_row[BookmarkColumns.LAST_ACCESSED] = now.to_unix()

    def __init__(self, parent):
        self._parent = parent
        icon_view = self._enquiry("delta > icon view")
        icon_view.connect("item-activated", self._on_item_activated)
