
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_web import InterPageSignals


class DeltaSearchEntry(Gtk.SearchEntry, DeltaEntity):

    def _on_activate(self, entry):
        text = entry.get_text()
        if not text:
            return
        uri = "https://duckduckgo.com/?q={}&ia=web".format(text)
        param = InterPageSignals.CREATE_WEB_PAGE, uri
        self._raise("delta > inter page signal", param)

    def __init__(self, parent):
        self._parent = parent
        Gtk.SearchEntry.__init__(self, margin=Unit(4))
        self.connect("activate", self._on_activate)
        self._raise("delta > add to container", self)
