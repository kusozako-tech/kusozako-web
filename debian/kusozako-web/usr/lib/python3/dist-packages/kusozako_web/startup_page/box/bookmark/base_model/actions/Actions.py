
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .BookmarkWebPage import DeltaBookmarkWebPage
from .Finalize import DeltaFinalize


class EchoActions:

    def __init__(self, parent):
        DeltaBookmarkWebPage(parent)
        DeltaFinalize(parent)
