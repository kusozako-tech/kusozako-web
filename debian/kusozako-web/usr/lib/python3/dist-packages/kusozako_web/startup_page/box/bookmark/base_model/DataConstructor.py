
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import WebKit2
from libkusozako3.Entity import DeltaEntity
from kusozako_web import InterPageSignals

ICON_SIZE = 64
ICON_THEME = Gtk.IconTheme.get_default()
LOOKUP_FLAG = Gtk.IconLookupFlags.FORCE_SIZE
DEFAULT_PIXBUF = ICON_THEME.load_icon("emblem-web", ICON_SIZE, LOOKUP_FLAG)


class DeltaDataConstructor(DeltaEntity):

    def _on_finished(self, favicon_database, task, param):
        try:
            cairo_surface = favicon_database.get_favicon_finish(task)
            width = cairo_surface.get_width()
            height = cairo_surface.get_height()
            rectangle = 0, 0, width, height
            pixbuf = Gdk.pixbuf_get_from_surface(cairo_surface, *rectangle)
            width = width*max(ICON_SIZE//width, 1/max(1, width//ICON_SIZE))
            height = height*max(ICON_SIZE//height, 1/max(1, height//ICON_SIZE))
            pixbuf = pixbuf.scale_simple(height, width, 0)
        except GLib.Error:  # when no favicon in database
            pixbuf = DEFAULT_PIXBUF.copy()
        uri, title = param
        tree_row_data = pixbuf, None, uri, title, 0
        self._raise("delta > append tree row data", tree_row_data)

    def construct(self, param):
        uri, _ = param
        self._favicon_database.get_favicon(uri, None, self._on_finished, param)

    def __init__(self, parent):
        self._parent = parent
        web_context = WebKit2.WebContext.get_default()
        self._favicon_database = web_context.get_favicon_database()
