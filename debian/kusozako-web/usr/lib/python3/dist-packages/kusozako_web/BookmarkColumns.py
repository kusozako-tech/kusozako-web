
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject
from gi.repository import GdkPixbuf

PIXBUF = 0
GROUP = 1
URI = 2
TITLE = 3
LAST_ACCESSED = 4

TYPES = (
    GdkPixbuf.Pixbuf,       # 0: PIXBUF
    str,                    # 1: GROUP
    str,                    # 2: URI
    str,                    # 3: TITLE
    GObject.TYPE_LONG,      # 4: LAST_ACCESSED
    )
