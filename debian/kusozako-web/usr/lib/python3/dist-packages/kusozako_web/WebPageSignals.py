
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

LOAD_COMMITED = 0           # progress
LOAD_FINISHED = 1           # title(str)
BOOKMARK_THIS_TAB = 2       # None
