
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib

CACHE_DIR = GLib.build_filenamev([GLib.get_user_cache_dir(), "kusozako-web"])
BOOKMARK_FILE = GLib.build_filenamev([CACHE_DIR, "bookmark"])
FAVICON_DIRECTORY = GLib.build_filenamev([CACHE_DIR, "favicon"])
