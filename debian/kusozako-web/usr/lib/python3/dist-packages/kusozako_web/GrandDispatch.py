
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .web_context.WebContext import DeltaWebContext
from .notebook.Notebook import DeltaNotebook


class DeltaGrandDispatch(DeltaEntity):

    def _delta_call_register_inter_page_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_inter_page_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        DeltaWebContext(self)
        DeltaNotebook(self)
